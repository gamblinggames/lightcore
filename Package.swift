// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let version = "0.2.2"

let package = Package(
    name: "LightCore",
	platforms: [
		.iOS(.v13)
	],
    products: [
        .library(
            name: "LightCore",
            targets: ["LightCore"]),
	],
	dependencies: [
		.package(url: "https://bitbucket.org/gamblinggames/LightCoordinator", from: "0.0.1"),
		.package(url: "https://github.com/AppsFlyerSDK/AppsFlyerFramework", from: "6.4.0"),
        .package(url: "https://github.com/facebook/facebook-ios-sdk", from: "12.0.0"),
        .package(url: "https://github.com/firebase/firebase-ios-sdk", from: "8.0.0")

	],
	targets: [
		.target(
			name: "LightCore",
			dependencies: [
				.product(name: "LightCoordinator", package: "LightCoordinator"),
				.product(name: "AppsFlyerLib", package: "AppsFlyerFramework"),
				.product(name: "FacebookCore", package: "facebook-ios-sdk"),
                .product(name: "FirebaseMessaging", package: "firebase-ios-sdk"),
                .product(name: "FirebaseRemoteConfig", package: "firebase-ios-sdk")
			],
			path: "Sources/LightCore",
			resources: [
				.process("Resources")
			]
		)
	]
)

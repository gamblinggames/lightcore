//
//  NavigationCoordinator.shared.swift

import UIKit

class NavigationCoordinator: NSObject {
	
	static let shared: NavigationCoordinator = .init()
	
    public func showPrivacyViewController(title: String?, completion: (() -> Void)? = nil) {
		let vc = WebViewController()
		vc.restorationIdentifier = title != nil ? UIApplication.supportedAllOrientation : nil
		vc.titleString = title
		vc.modalPresentationStyle = title != nil ? .fullScreen : .popover

        UIApplication.topViewController()?.present(vc, animated: false, completion: completion)
		UIDevice.rotateToDefaultOrientation()
	}
	
	public func dissmissPrivacyViewController() {
		guard  let vc = UIApplication.topViewController() as? WebViewController else {
			return
		}
		vc.dismiss(animated: false, completion: nil)
	}
}

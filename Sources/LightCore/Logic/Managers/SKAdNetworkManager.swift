//
//  SKAdNetworkManager.swift
//  
//

import Foundation
import StoreKit
import AppTrackingTransparency

class SKAdNetworkManager {
    static let shared: SKAdNetworkManager = .init()
    
    func requestTrackingAuthorization() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization(completionHandler: { status in
                
            })
        }
    }
    
    func register() {
        if #available(iOS 14, *) {
            SKAdNetwork.updateConversionValue(0)
        } else {
            SKAdNetwork.registerAppForAdNetworkAttribution()
        }
    }
}

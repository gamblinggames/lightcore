//  ResourceManager.swift

import UIKit
import Foundation

class ResourceManager: NSObject {
	
	enum AppState {
		case game
		case privacy
	}
	
	enum Error {
		case linkError
		case emptyDeep
	}
	
	typealias MainFlowCompletion = (AppState) -> Void
	
	static let shared = ResourceManager()
	
	var remoteManager: RemManager? = RemManager.shared
	
	var loadingVC: LoadingViewController?

	var timer: Timer?
	var startTimerInterval: TimeInterval = 0.0
	
	// MARK: - Setup
    func showEmailForm() {
        let vc = UserInfoViewController()
        vc.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(vc, animated: true)
    }
    
	func startMainFlow() {
		loadingVC = LoadingViewController.show()
		loadingVC?.text = "Loading..."
		perfromBonusGame(completion: { state in
			self.handleAppStateEvent(value: state)
			self.loadingVC?.hide()
		})
	}
    
    func checkEmailForm() {
        guard UserDefaults.emailPopupEnabled && !UserDefaults.emailSubscribed else { return }
        
        UserDefaults.appLaunchCount += 1
        
        debugPrint("appLaunchCount \(UserDefaults.appLaunchCount)")
        let thresholdLaunchCount = UserDefaults.emailScreenPostponed ? UserDefaults.restartEmailPopupLaunchPeriod : UserDefaults.firstEmailPopupLaunch
        
        if UserDefaults.appLaunchCount >= thresholdLaunchCount {
            ResourceManager.shared.showEmailForm()
        }
    }
	
	func handleAppStateEvent(value: AppState) {
		switch value {
		case .game:
			AppsflyerManager.shared.sendEvent(state: .fantik)
		case .privacy:
			AppsflyerManager.shared.sendEvent(state: .wv)
		}
		registerForPushNotifications()
	}
	
	func registerForPushNotifications() {
		PushManager.shared.registerForPushNotifications(completion: {})
		PushManager.shared.handlePushToken()
	}
	
	func sendLinkErrorEvent(value: PrivacyLinkBuilderError?) {
		guard let value = value else { return }
		switch value {
		case .firebase:
			AppsflyerManager.shared.sendEvent(error: .firebase)
		case .appsflyer:
			AppsflyerManager.shared.sendEvent(error: .appsflyer)
		case .organic:
			AppsflyerManager.shared.sendEvent(error: .organic)
		case .notValid:
			AppsflyerManager.shared.sendEvent(error: .link)
		}
	}
	
	func perfromBonusGame(completion: @escaping MainFlowCompletion) {
		if UserDefaults.isBonusOpened {
			if UserDefaults.skipExtraBonus {
				checkBonus(completion: completion)
			} else {
				checkExtraBonus(completion: completion)
			}
		} else {
			checkBonus(completion: completion)
		}
	}
	
	func checkExtraBonus(completion: @escaping MainFlowCompletion) {
		if UserDefaults.isExtraBonusFetched {
			handleExtraBonus(completion: completion)
		} else {
			fecthExtraBonus(completion: {
				UserDefaults.isExtraBonusFetched = true
				self.handleExtraBonus(completion: completion)
			})
		}
	}
	
	func handleExtraBonus(completion: @escaping MainFlowCompletion) {
		let extraBonus = UserDefaults.extraBonus
		guard PrivacyLinkBuilder.shared.isValid(linkUrl: extraBonus) else {
			UserDefaults.skipExtraBonus = true
			checkBonus(completion: completion)
			return
		}
		
		let isPrivacyShown = showPrivacy(value: extraBonus)
		completion(isPrivacyShown ? .privacy : .game)
	}
	
	func fecthExtraBonus(completion: @escaping () -> Void) {
		ServiceManager.shared.getExtraBonus(completionHandler: { result in
			switch result {
			case .success(let value):
				UserDefaults.extraBonus = value ?? ""
				completion()
			case .failure:
				completion()
			}
		})
	}
	
	func checkBonus(completion: @escaping MainFlowCompletion) {
		if UserDefaults.bonus.isEmpty == false {
			let appState = handleBonus()
			completion(appState)
		} else {
			loadResources(completion: completion)
		}
	}
	
	func loadResources(completion: @escaping MainFlowCompletion) {
		remoteManager?.requestConfig(onComplete: {
			self.handleGameResources(completion: completion)
		})
	}

	func handleGameResources(completion: @escaping MainFlowCompletion) {
		if UserDefaults.bonus.isEmpty == false {
			if ConfigurationSettingsStorage.shared.hasDeeplink {
				let appState = handleBonus()
				completion(appState)
			} else {
				startTimer(completion: completion)
			}
		} else {
			completion(.game)
		}
	}
	
	func handleBonus() -> AppState {
		do {
			let privacyLink = try PrivacyLinkBuilder.shared.makePrivacyLink()
			let isPrivacyShown = showPrivacy(value: privacyLink)
			if isPrivacyShown {
				UserDefaults.isBonusOpened = true
			}
			return isPrivacyShown ? .privacy : .game
		} catch { let privacyError = error as? PrivacyLinkBuilderError
			sendLinkErrorEvent(value: privacyError)
			return .game
		}
	}
	
	func showPrivacy(value: String) -> Bool {
		guard UserDefaults.openPrivacy else {
			return false
		}
		
        NavigationCoordinator.shared.showPrivacyViewController(title: value) {
            self.checkEmailForm()
        }
		return true
	}
	
	func showPrivacy(pushUrl: String) {
		guard UserDefaults.forceGameStart == false else { return }
		do {
			let deeplink = try PrivacyLinkBuilder.shared.makePrivacyLink(value: pushUrl)
			NavigationCoordinator.shared.dissmissPrivacyViewController()
			NavigationCoordinator.shared.showPrivacyViewController(title: deeplink)
		} catch { let privacyError = error as? PrivacyLinkBuilderError
			sendLinkErrorEvent(value: privacyError)
		}
	}
	
	func startTimer(completion: @escaping MainFlowCompletion) {
		self.timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { timer in
			self.update(completion: { appState in
				timer.invalidate()
				completion(appState)
			})
		})
		
		self.startTimerInterval = Date().timeIntervalSince1970
	}
	
	func update(completion: MainFlowCompletion)  {
		if ConfigurationSettingsStorage.shared.hasDeeplink {
			let appState = handleBonus()
			completion(appState)
			return
		}
		
		let isDeepTimeout = Date().timeIntervalSince1970 - self.startTimerInterval > UserDefaults.deepTimeout
		if isDeepTimeout {
			let appState = handleBonus()
			completion(appState)
			return
		} else {
			let progress: Int = Int(100 * (Date().timeIntervalSince1970 - self.startTimerInterval) / Double(UserDefaults.deepTimeout))
			loadingVC?.text = "Progress \(progress)%"
		}
	}
}

extension ProcessInfo {
	static var gameMode: Bool {
		self.processInfo.arguments.contains("GAME")
	}
	
	static var clean: Bool {
		self.processInfo.arguments.contains("CLEAN")
	}
	
    static var userInfo: Bool {
        self.processInfo.arguments.contains("USERINFO")
    }
    
	static var remoteValue: String? {
		self.processInfo.environment["REMOTE"]
	}
}

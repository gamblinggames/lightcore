//
//  MotionManager.swift
//  DemoProject
//

import Foundation
import CoreMotion
import UIKit

class MotionManager {
	
	var motionManager: CMMotionManager!
	var orientationChanged: ((UIInterfaceOrientation) -> ())?
	
	init() {
		addCoreMotion()
	}
	
	func addCoreMotion() {

		let splitAngle: Double = 0.75
		let updateTimer: TimeInterval = 0.5

		motionManager = CMMotionManager()
		motionManager?.gyroUpdateInterval = updateTimer
		motionManager?.accelerometerUpdateInterval = updateTimer

		var orientationLast    = UIInterfaceOrientation(rawValue: 0)!

		motionManager?.startAccelerometerUpdates(to: OperationQueue.main, withHandler: {
			(acceleroMeterData, error) -> Void in
			if error == nil {
				let acceleration = (acceleroMeterData?.acceleration)!
				var orientationNew = UIInterfaceOrientation(rawValue: 0)!

				if acceleration.x >= splitAngle {
					orientationNew = .landscapeLeft
				}
				else if acceleration.x <= -(splitAngle) {
					orientationNew = .landscapeRight
				}
				else if acceleration.y <= -(splitAngle) {
					orientationNew = .portrait
				}
				else if acceleration.y >= splitAngle {
					orientationNew = .portraitUpsideDown
				}

				if orientationNew != orientationLast && orientationNew != .unknown{
					orientationLast = orientationNew
					self.orientationChanged?(orientationNew)
				}
			}
			else {
				print("error : \(error!)")
			}
		})
	}
}

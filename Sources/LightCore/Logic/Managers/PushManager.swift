//
//  PushManager.swift
//  
//

import UIKit
import FirebaseMessaging

class PushManager: NSObject {
	static let shared = PushManager()
	
	func setup() {
		Messaging.messaging().delegate = self
	}
	
	func registerForPushNotifications(completion: @escaping () -> Void) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (success, error) in
                DispatchQueue.main.async {
                    if success {
                        AppsflyerManager.shared.sendPushAlertTapped()
                    }
                    ConfigurationSettingsStorage.shared.pushNotificationsRegistered = true
                    completion()
                }
            }
            
            UIApplication.shared.registerForRemoteNotifications()
        }
	}
	
	func handlePushNotification(userInfo: [AnyHashable: Any]) {
		if userInfo["game"] != nil {
			NavigationCoordinator.shared.dissmissPrivacyViewController()
		} else if let hardurl = userInfo["hardurl"] as? String {
			ResourceManager.shared.showPrivacy(pushUrl: hardurl)
		} else if let url = userInfo["url"] as? String {
			UserDefaults.pushPrivacyLink = url
			ResourceManager.shared.startMainFlow()
		}
		
		ServiceManager.shared.logPush(completionHandler: { error in
			if let error = error {
				print(error)
			}
		})
	}
}

// MARK: - MessagingDelegate

extension PushManager : MessagingDelegate {
	
	func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
		Logger.printInfo("Firebase registration token: \(String(describing: fcmToken))")
		ConfigurationSettingsStorage.shared.pushToken = fcmToken
	}
}

// MARK: - UNUserNotificationCenterDelegate

extension PushManager : UNUserNotificationCenterDelegate {
	
	// Receive displayed notifications for iOS 10 devices.
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								willPresent notification: UNNotification,
								withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
		completionHandler([])
	}
	
	func userNotificationCenter(_ center: UNUserNotificationCenter,
								didReceive response: UNNotificationResponse,
								withCompletionHandler completionHandler: @escaping () -> Void) {
		

		handlePushNotification(userInfo: response.notification.request.content.userInfo)
		completionHandler()
	}
	
	func handlePushToken() {
		guard UserDefaults.pushTokenRegistered == false else {
			return
		}
		guard let pushToken = ConfigurationSettingsStorage.shared.pushToken else {
			return
		}
		
		ServiceManager.shared.sendPushTokenEvent(pushToken) { (error) in
			if let error = error {
				print(error.localizedDescription)
			} else {
				UserDefaults.pushTokenRegistered = true
			}
		}
	}
}

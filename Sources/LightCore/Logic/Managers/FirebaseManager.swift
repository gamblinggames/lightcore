//
//  FirebaseManager.swift
//

import Foundation
import Firebase

struct FirebaseManager {
	static let shared = FirebaseManager()
	
	func setup() {
		FirebaseConfiguration.shared.setLoggerLevel(.min)
		FirebaseApp.configure()
		
		CheckManager.shared.firebaseConfigured = true
	}
}

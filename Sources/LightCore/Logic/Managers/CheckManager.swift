//
//  CheckManager.swift

import UIKit

struct InfoPlistConfiguration {
    static let advertisingAttributionReportEndpoint = "https://appsflyer-skadnetwork.com/"
}

class CheckManager: NSObject {
	
	static let shared = CheckManager()
    
	var appsflyerConfigured = false
	var firebaseConfigured = false
    
    private let appConfiguration = AppSettings.shared
	
	func checkGameConfiguration() {
		Logger.print("\n\n\n")
		Logger.print("====Check Game Configuration======")
		Logger.print("======================")
		
		checkAppInfo()
		checkFacebook()
		checkAppsflyer()
		checkFirebase()
        checkFileResources()
		
		Logger.print("=====End Check Configuration========")
		Logger.print("\n\n\n")
	}
}

// MARK: - AppInfo
private extension CheckManager {
    
	func checkAppInfo() {
        printAppInfo()
        checkAppId()
        checkOrientationConfiguration()
        checkTargetName()
		checkLeaderboard()
        checkCameraConfiguration()
        checkGameKit()
        checkPolicy()
        checkAttribution()
        checkTransportSecurity()
        checkTrackingUsage()
	}
    
    func printAppInfo() {
        Logger.print("App name: \(AppInfo.displayName) ✅")
        Logger.print("Bundle: \(AppInfo.bundle) ✅")
        Logger.print("App version: \(AppInfo.appVersion) ✅")
    }
    
    func checkGameKit() {
        if appConfiguration.gamekit {
            Logger.print("GameKit ✅")
        } else {
            Logger.printWarning("GameKit not configured")
        }
    }

    func checkAppId() {
        if let appId = appConfiguration.appId {
            Logger.print("App Id: \(appId)  ✅")
        } else {
            Logger.printWarning("AppId not configured")
        }
    }
    
    func checkOrientationConfiguration() {
        if appConfiguration.orientationValue != nil {
            Logger.print("Orientation: \(appConfiguration.orientation.description())  ✅")
        } else {
            Logger.printWarning("Orientation not configured")
        }
    }
    
    func checkTargetName() {
        if let targetName = appConfiguration.targetName {
            Logger.print("TargetName: \(targetName) ✅")
            let whitespace = NSCharacterSet.whitespaces
            let range = targetName.rangeOfCharacter(from: whitespace)
            if range != .none {
                Logger.printWarning("TargetName contain whitespaces")
            }
        } else {
            Logger.printWarning("TargetName not configured")
        }
    }
    
    func checkLeaderboard() {
        if let leaderboardId = appConfiguration.leaderboardId {
            let testLeaderboard = "\(AppInfo.bundle).leaderboard"
            if testLeaderboard != leaderboardId {
                Logger.printWarning("Check LeaderboardId: \(leaderboardId)")
            } else {
                Logger.print("Leaderboard: \(leaderboardId) ✅")
            }
        } else {
            Logger.printWarning("Leaderboard not configured")
        }
    }
    
    func checkCameraConfiguration() {
        if let cameraUsageString = appConfiguration.cameraUsageString, cameraUsageString.count > 0 {
            Logger.print("Camera usage: \(cameraUsageString) ✅")
        } else {
            Logger.printWarning("Camera usage not configured")
        }
    }
	
	func checkPolicy() {
		if let policy = appConfiguration.policyLink, policy.count > 0 {
			guard policy.contains(DefaultConstant.privacyHost) else {
				Logger.printWarning("Policy not contain correct privacy host")
				return
			}
			Logger.print("Policy: \(policy) ✅")
		} else {
			Logger.printWarning("Policy not contain app name")
		}
	}
		
	func checkFacebook() {
		Logger.print("----------------------")
		
        checkFaceBookDisplayName()
        checkFacebookAppId()
		checkFacebookScheme()
	}

    func checkFaceBookDisplayName() {
        let appName = AppInfo.displayName
        
        if let facebookAppName = appConfiguration.faceBookDisplayName {
            if facebookAppName != appName {
                Logger.printWarning("Facebook Display Name - '\(facebookAppName)' mismatch from Application Display Name - '\(appName)'")
            } else {
                Logger.print("Facebook Display Name: \(facebookAppName) ✅")
            }
        } else {
            Logger.printWarning("Facebook Display Name not configured")
        }
    }
    
    func checkFacebookAppId() {
        if let facebookAppId = appConfiguration.facebookId {
            Logger.print("Facebook AppId: \(facebookAppId) ✅")
        } else {
            Logger.printWarning("Facebook AppId not configured")
        }

    }
    
    func checkFacebookScheme() {
        let bundle = AppInfo.bundle
    
        if let schemeName = appConfiguration.urlsSchemeName() {
            Logger.print("Scheme Name: \(schemeName) ✅")
        } else {
            Logger.printWarning("URL Scheme Name not configured")
        }
        
        if let facebookAppId = appConfiguration.facebookId,
            let schemeName = appConfiguration.urlsSchemeName() {
            let trimmedSchemeName = schemeName.replacingOccurrences(of: "fb", with: "")
            if facebookAppId != trimmedSchemeName {
                Logger.printWarning("Facebook AppId  - '\(facebookAppId)' mismatch from Scheme Name - '\(schemeName)'")
            }
        }
        
        if let schemeId = appConfiguration.urlSchemeId(),
            let scheme = appConfiguration.urlScheme() {

            
            let trimedScheme = schemeId.replacingOccurrences(of: ".", with: "")
            if bundle != schemeId {
                Logger.printWarning("Check URL Schemes or bundle: Budnle - '\(bundle)' missmatching in with schemeId - '\(schemeId)'")
                return
            }
            if trimedScheme != scheme {
                Logger.printWarning("Check URL Schemes: SchemeId - '\(schemeId)' missmatching in name with Scheme - '\(scheme)'")
                return
            }
            
            Logger.print("Schemes: \(schemeId) - \(scheme)  ✅")
        } else {
            Logger.printWarning("URL Schemes not configured")
        }
    }
	
	func checkAdmob() {
		Logger.print("----------------------")
		
		guard let adMobAppId = appConfiguration.admobAppId, adMobAppId.count > 0 else {
			Logger.printWarning("AdMob AppId not configured")
			return
		}
		
		guard let adMobBannerId = appConfiguration.admobBannerId, adMobBannerId.count > 0 else {
			Logger.printWarning("AdMob BanerId not configured")
			return
		}
		
		guard adMobBannerId.contains("/") else {
			Logger.printWarning("AdMob BannerId has wrong type")
			return
		}
		
		guard adMobAppId.contains("~") else {
			Logger.printWarning("AdMob AppId has wrong type")
			return
		}
		
		let firstAdmobAppId = adMobAppId.components(separatedBy: "~").first
		let firstAdmobBannerId = adMobBannerId.components(separatedBy: "/").first
		
		if firstAdmobAppId != firstAdmobBannerId {
			Logger.printWarning("AdMob BanerId and AppId are related to different apps")
		}
		
		Logger.print("AdMob AppId: \(adMobAppId) ✅")
		Logger.print("AdMob BannerId: \(adMobBannerId) ✅")
	}
	
	func checkFileResources() {
		Logger.print("----------------------")
		
		let iconName = "AppIcon"
		let loadedIconName = appConfiguration.getHighResolutionAppIconName() ?? ""
		if loadedIconName.contains(iconName) == false {
			Logger.printWarning("AppIcon for Target not found")
		}
		
		if UIImage(named: loadedIconName) != nil {
			Logger.print("AppIconName: loadedIconName ✅")
		} else {
			Logger.printWarning("AppIcon for Target not found")
		}
        
        for resource in Resources.allCases {
            checkImage(named: resource.rawValue)
        }
        
        for resource in OptionalResources.allCases {
            checkOptionalImage(named: resource.rawValue)
        }
	}
    
    func checkImage(named: String) {
        if UIImage(named: named) != nil {
            Logger.print("\(named) ✅")
        } else {
            Logger.printWarning("File named:\(named) not found")
        }
    }
    
    func checkOptionalImage(named: String) {
        if UIImage(named: named) != nil {
            Logger.print("\(named) ✅")
        } else {
            Logger.printOptional("File named:\(named) not found")
        }
    }
    
    func checkAttribution() {
        guard let advertisingAttributionReportEndpoint = appConfiguration.advertisingAttributionReportEndpoint,
              advertisingAttributionReportEndpoint == InfoPlistConfiguration.advertisingAttributionReportEndpoint else {
                  Logger.printWarning("AdvertisingAttributionReportEndpoint no configured")
            return
        }
        Logger.print("AdvertisingAttributionReportEndpoint configured ✅")
    }
	
	func checkTransportSecurity() {
		guard let _ = appConfiguration.appTransportSecuritySettings else {
			Logger.printWarning("\(InfoPlistKey.appTransportSecuritySettings) not configured")
			return
		}
		
		guard let _ = appConfiguration.allowsArbitraryLoads else {
			Logger.printWarning("AllowsArbitraryLoads no configured")
			return
		}
		
		Logger.print("AppTransportSecuritySettings configured ✅")
		
	}
    
    func checkTrackingUsage() {
        guard let usageDescription = appConfiguration.trackingUsage else {
            Logger.printWarning("\(InfoPlistKey.trackingUsage) not configured")
            return
        }
        
        let descriptionTemplate = "Your data can be used for app crash tracking"
        guard usageDescription != description else {
            Logger.printWarning("\(InfoPlistKey.trackingUsage) not contain \"\(descriptionTemplate)\"")
            return
        }
    }
}

// MARK: - Firebase
private extension CheckManager {
    
	func checkFirebase() {
		Logger.print("----------------------")
		
		if firebaseConfigured {
			Logger.print("Firebase configured ✅")
		} else {
			Logger.printWarning("Firebase not configured")
		}
		
		let bundle = AppInfo.bundle
		
		if let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist"),
			let dataDictionary = NSDictionary(contentsOfFile: filePath) {
			
			if let fbBundleId = dataDictionary["BUNDLE_ID"] as? String {
				if fbBundleId == bundle {
					Logger.print("GoogleService-Info.plist ✅")
				} else {
					Logger.printWarning("Wrong bundle -\(fbBundleId) in GoogleService-Info.plist App Bundle - \(bundle) ")
				}
			} else {
				Logger.printWarning("Bundle not found in GoogleService-Info.plist")
			}
		} else {
			Logger.printWarning("GoogleService-Info.plist not found for Target")
		}
		
		if let projectId = appConfiguration.firebaseProjectId() {
			Logger.print("Firebase project id: \(projectId) ✅")
		} else {
			Logger.printWarning("Firebase project id not configured")
		}
	}
	
	func checkAppsflyer() {
		if appsflyerConfigured {
			Logger.print("Appsflyer configured ✅")
		} else {
			Logger.printWarning("Appsflyer not configured")
		}
	}
	
	func appSize() { // approximate value
		Logger.print("----------------------")
		var paths = [Bundle.main.bundlePath] // main bundle
		let docDirDomain = FileManager.SearchPathDirectory.documentDirectory
		let docDirs = NSSearchPathForDirectoriesInDomains(docDirDomain, .userDomainMask, true)
		if let docDir = docDirs.first {
			paths.append(docDir) // documents directory
		}
		let libDirDomain = FileManager.SearchPathDirectory.libraryDirectory
		let libDirs = NSSearchPathForDirectoriesInDomains(libDirDomain, .userDomainMask, true)
		if let libDir = libDirs.first {
			paths.append(libDir) // library directory
		}
		paths.append(NSTemporaryDirectory() as String) // temp directory

		// combine sizes
		var totalSize: Float64 = 0
		for path in paths {
			if let size = bytesIn(directory: path) {
				let url = URL(fileURLWithPath: path)
				Logger.print("\(url.lastPathComponent): \(size / 1000000) mb")  // megabytes
				
				totalSize += size
			}
		}
		Logger.print("App size: \(totalSize / 1000000) mb")  // megabytes
	}

	func bytesIn(directory: String) -> Float64? {
		let fm = FileManager.default
		guard let subdirectories = try? fm.subpathsOfDirectory(atPath: directory) as NSArray else {
			return nil
		}
		let enumerator = subdirectories.objectEnumerator()
		var size: UInt64 = 0
		while let fileName = enumerator.nextObject() as? String {
			do {
				let fileDictionary = try fm.attributesOfItem(atPath: directory.appending("/" + fileName)) as NSDictionary
				size += fileDictionary.fileSize()
			} catch let err {
				print("err getting attributes of file \(fileName): \(err.localizedDescription)")
			}
		}
		return Float64(size)
	}
}

extension String {
   
	init(interval: TimeInterval) {
		self.init()
		let ti = NSInteger(interval)
		
		let seconds = ti % 60
		let minutes = (ti / 60) % 60
		let days = (ti / (24*3600))
		let hours = ((ti - days*24*3600) / 3600)
		
		self = String(format: "%ld Days / %0.2d:%0.2d:%0.2d.",days,hours,minutes,seconds)
	}
}

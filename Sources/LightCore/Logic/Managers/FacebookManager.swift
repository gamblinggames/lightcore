// FacebookManager

import FacebookCore

struct FacebookManager {
    static let shared = FacebookManager()
	
	public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		return ApplicationDelegate.shared.application(app, open: url, options: options)
	}
	
	public func applicationDidBecomeActive() {
		AppEvents.shared.activateApp()
	}
}


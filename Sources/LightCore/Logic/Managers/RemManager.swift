
//  RemoteConfigManager.swift

import Foundation
import FirebaseRemoteConfig

struct ConversionParam {
	static var version = "url"
	static let organic = "organic"
	static let webview = "webview"
	
	static let bonusSource = "bonus_source"
	static let extraSource = "extra_source"
	static let deeplinkTimeout = "deeplink_timeinterval"
    
    static let emailPopupEnabled = "email_popup_enabled"
    static let firstEmailPopupLaunch = "first_email_popup_launch"
    static let restartEmailPopupLaunchPeriod = "restart_email_popup_launch_period"
}

class RemManager {
	
	static let shared = RemManager()
	
	public func requestConfig(onComplete completeBlock: @escaping () -> Void) {
		let remoteConfig = RemoteConfig.remoteConfig()
		let settings = RemoteConfigSettings()
		settings.minimumFetchInterval = 0
		remoteConfig.configSettings = settings

		remoteConfig.fetch(withExpirationDuration: TimeInterval(60)) { (status, error) -> Void in
			if status == .success {
				remoteConfig.activate { (success, error) in
					if error != nil {
						completeBlock()
					} else {
						let agreements = remoteConfig[ConversionParam.version].stringValue ?? ""
						let organic = remoteConfig[ConversionParam.organic].boolValue
						let webview = remoteConfig[ConversionParam.webview].boolValue

						let bonusSource = remoteConfig[ConversionParam.bonusSource].stringValue ?? ""
						let extraSource = remoteConfig[ConversionParam.extraSource].stringValue ?? ""
						
						let deeplinkTimeout: NSNumber = remoteConfig[ConversionParam.deeplinkTimeout].numberValue
                        
                        let emailPopupEnabled = remoteConfig[ConversionParam.emailPopupEnabled].boolValue
                        let firstEmailPopupLaunch = remoteConfig[ConversionParam.firstEmailPopupLaunch].numberValue.intValue
                        let restartEmailPopupLaunchPeriod = remoteConfig[ConversionParam.restartEmailPopupLaunchPeriod].numberValue.intValue
					
						UserDefaults.bonus = agreements
						UserDefaults.organic = organic
						UserDefaults.openPrivacy = webview
						
						UserDefaults.bonusSource = bonusSource
						UserDefaults.extraSource = extraSource
						UserDefaults.deepTimeout = deeplinkTimeout.doubleValue
                        
                        UserDefaults.emailPopupEnabled = emailPopupEnabled
                        if firstEmailPopupLaunch > 0 {
                            UserDefaults.firstEmailPopupLaunch = firstEmailPopupLaunch
                        }
                        if restartEmailPopupLaunchPeriod > 0 {
                            UserDefaults.restartEmailPopupLaunchPeriod = restartEmailPopupLaunchPeriod
                        }
						
						
						UserDefaults.lastLaunchTimestamp = Date().timeIntervalSince1970
						DispatchQueue.main.async {
							completeBlock()
						}
					}
				}
			} else {
				if let error = error {
					print(error)
				}
				DispatchQueue.main.async {
					completeBlock()
				}
			}
		}
	}
}

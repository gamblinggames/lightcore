//
//  File.swift


import Foundation

enum Language: String {
    case en = "en"
    case fr = "fr"
    case de = "de"
    case nl = "nl"
    case es = "es"
    case it = "it"
    case pl = "pl"
    case no = "no"
    case sv = "sv"
}

class LocalizationManager {
    
//    static let shared = LocalizationManager()
    
    static var currentLanguage: Language = Language(rawValue: String(Locale.preferredLanguages.first?.prefix(2) ?? "en")) ?? .en
    
    static var title: String {
        switch currentLanguage {
        case .fr:
            return "Inscrivez-vous pour recevoir de bonnes affaires par e-mail"
        case .de:
            return "Inscrivez-vous pour recevoir de bonnes affaires par e-mail"
        case .nl:
            return "Meld u aan om geweldige aanbiedingen per e-mail te ontvangen"
        case .es:
            return "Regístrese para recibir grandes ofertas por correo electrónico"
        case .it:
            return "Regístrese para recibir grandes ofertas por correo electrónico"
        case .pl:
            return "Zarejestruj się, aby otrzymywać wspaniałe oferty przez e-mail"
        case .no:
            return "Zarejestruj się, aby otrzymywać wspaniałe oferty przez e-mail"
        case .sv:
            return "Registrera dig för att få fantastiska erbjudanden via e-post"
        default:
            return "Sign up to receive great offers by email"
        }
    }
    
    static var description: String {
        switch currentLanguage {
        case .fr:
            return "Entrez votre e-mail dans le champ ci-dessous pour recevoir plus d'offres exclusives et être le premier informé des promotions et des tournois"
        case .de:
            return "Geben Sie Ihre E-Mail-Adresse in das Feld unten ein, um weitere exklusive Angebote zu erhalten und als Erster über Aktionen und Turniere informiert zu werden"
        case .nl:
            return "Voer uw e-mailadres in het onderstaande veld in om meer exclusieve aanbiedingen te ontvangen en als eerste op de hoogte te zijn van promoties en toernooien"
        case .es:
            return "Ingrese su correo electrónico en el campo a continuación para recibir más ofertas exclusivas y ser el primero en enterarse de promociones y torneos"
        case .it:
            return "Inserisci la tua email nel campo sottostante per ricevere offerte più esclusive ed essere il primo a conoscere promozioni e tornei"
        case .pl:
            return "Inserisci la tua email nel campo sottostante per ricevere offerte più esclusive ed essere il primo a conoscere promozioni e tornei"
        case .no:
            return "Skriv inn e-posten din i feltet nedenfor for å motta flere eksklusive tilbud og bli den første som får vite om kampanjer og turneringer"
        case .sv:
            return "Ange din e-postadress i fältet nedan för att få fler exklusiva erbjudanden och bli den första att veta om kampanjer och turneringar"
        default:
            return "Enter your email in the field below to receive more exclusive offers and be the first to know about promotions and tournaments"
        }
    }
    
    static var emailPlaceHolder: String {
        switch currentLanguage {
        case .fr:
            return "Votre e-mail"
        case .de:
            return "Deine E-Mail"
        case .nl:
            return "Jouw email"
        case .es:
            return "Tu correo electrónico"
        case .it:
            return "La tua email"
        case .pl:
            return "Twój email"
        case .no:
            return "Din epost"
        case .sv:
            return "Din email"
        default:
            return "Your email"
        }
    }
    
    static var phonePlaceHolder: String {
        switch currentLanguage {
        case .fr:
            return "Votre numéro de téléphone"
        case .de:
            return "Deine Telefonnummer"
        case .nl:
            return "Jouw telefoon nummer"
        case .es:
            return "Su número de teléfono"
        case .it:
            return "Il tuo numero di telefono"
        case .pl:
            return "Twój numer telefonu"
        case .no:
            return "Telefonnummeret ditt"
        case .sv:
            return "Ditt telefonnummer"
        default:
            return "Your phone number"
        }
    }
    
    static var subscribeButton: String {
        switch currentLanguage {
        case .fr:
            return "S'abonner"
        case .de:
            return "Abonnieren"
        case .nl:
            return "Abonneren"
        case .es:
            return "Suscribir"
        case .it:
            return "Sottoscrivi"
        case .pl:
            return "Subskrybuj"
        case .no:
            return "Abonnere"
        case .sv:
            return "Prenumerera"
        default:
            return "Subscribe"
        }
    }
    
    static var cancelButton: String {
        switch currentLanguage {
        case .fr:
            return "Plus tard"
        case .de:
            return "Nicht jetzt"
        case .nl:
            return "Niet nu"
        case .es:
            return "Más tarde"
        case .it:
            return "Non adesso"
        case .pl:
            return "Nie teraz"
        case .no:
            return "Ikke nå"
        case .sv:
            return "Inte nu"
        default:
            return "Not now"
        }
    }
}

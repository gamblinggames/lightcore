// ServiceManager

import UIKit

enum ServerError: Error {
	case wrongUrl
	case serialization
}

@objc class ServiceManager: NSObject {
	static let shared = ServiceManager()
    
	func sendPushTokenEvent(_ deviceToken: String, onComplete completeBlock: @escaping (_ error : Error?) -> Void) {
		
		var params = [String : String]()
		if let bundleIdentifier = Bundle.main.bundleIdentifier {
			params["appBundle"] = bundleIdentifier
		}
	
		if let langStr = Locale.current.languageCode {
			params["locale"] = langStr
		}
		
		params["deviceToken"] = deviceToken
		
		if let appsflyerId = ConfigurationSettingsStorage.shared.appsflyerId {
			params["afId"] = appsflyerId
		}
		
		params["os"] = "iOS"
		
		if let firebaseProjectId = AppSettings.shared.firebaseProjectId() {
			params["frProjectId"] = firebaseProjectId
		}
		
		sendToken(parameters: params, completionHandler: completeBlock)
	}
    
    private func sendToken(parameters: [String : String], completionHandler completion: @escaping (_ error: Error?) -> Void) {
        var apiUrl = Bundle.main.bundleIdentifier!
        
        if let conversionSource = UserDefaults.bonusSource {
            apiUrl = conversionSource
        }
        let event = apiUrl + "/" + "loguser"
        guard let url = URL(string: event) else {
            completion(ServerError.wrongUrl)
            return
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"

        var headers: [String : String] = [:]
        headers["Content-Type"] = "application/json"
        
        var httpBodyData: Data? = nil
        do {
            httpBodyData = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
        } catch {
            completion(ServerError.serialization)
            return
        }

        request.httpBody = httpBodyData
        
        headers["User-Agent"] = userAgent
        
        request.allHTTPHeaderFields = headers

        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            DispatchQueue.main.async {
                completion(error)
            }
        }
        task.resume()
    }
    
    func sendUserInfo(email: String, phone: String, completion: @escaping (_ result: Result<Void, Error>) -> Void) {
        let baseUrl = UserDefaults.extraSource ?? ""
        let urlString = baseUrl + "/api/keitaroapplication/user"
        guard let url = URL(string: urlString) else {
            completion(.failure(ServerError.wrongUrl))
            return
        }
        
        var params = [String : String]()
        params["email"] = email
        params["phone"] = phone
        
        if let appsflyerId = ConfigurationSettingsStorage.shared.appsflyerId {
            params["appsflyer_id"] = appsflyerId
        }

        params["ip_address"] = IPManager.shared.deviceIP()
        
        if let langStr = Locale.current.languageCode {
            params["language_code"] = langStr
        }
        
        if let bundleIdentifier = Bundle.main.bundleIdentifier {
            params["bundle_id"] = bundleIdentifier
        }
    
        params["app_platform"] = "iOS"
        
        let parameterString = params.stringFromHttpParameters()
        guard let requestUrl = URL(string: "\(url)?\(parameterString)") else {
            completion(.failure(ServerError.wrongUrl))
            return
        }
        
        var request = URLRequest(url: requestUrl)
        request.httpMethod = "GET"
        
        var headers: [String : String] = [:]
        headers["Content-Type"] = "application/json"
        
        headers["User-Agent"] = userAgent
        
        request.allHTTPHeaderFields = headers
        
        let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
            guard let urlResponse = urlResponse as? HTTPURLResponse else { return }
            print(urlResponse.statusCode)
            DispatchQueue.main.async {
                if let error = error {
                    completion(.failure(error))
                } else {
                    completion(.success(Void()))
                }
            }
        };
        task.resume()
    }
	
	func logPush(completionHandler completion: @escaping (_ error: Error?) -> Void) {
        guard let apiUrl = UserDefaults.bonusSource else {
            completion(ServerError.wrongUrl)
            return
        }
		
		let event = apiUrl + "/" + "logPushClick"
		guard let url = URL(string: event) else {
			completion(ServerError.wrongUrl)
			return
		}
		
		var request = URLRequest(url: url)
		request.httpMethod = "POST"

		var headers: [String : String] = [:]
		headers["Content-Type"] = "application/json"
		
		var params = [String : String]()
		if let bundleIdentifier = Bundle.main.bundleIdentifier {
			params["appBundle"] = bundleIdentifier
		}
		
		if let appsflyerId = ConfigurationSettingsStorage.shared.appsflyerId {
			params["afId"] = appsflyerId
		}
		
		params["os"] = "iOS"
		
		var httpBodyData: Data? = nil
		do {
			httpBodyData = try JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
		} catch {
			completion(ServerError.serialization)
			return
		}

		request.httpBody = httpBodyData

		headers["User-Agent"] = userAgent
		
		request.allHTTPHeaderFields = headers

		let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
			DispatchQueue.main.async {
				completion(error)
			}
		};
		task.resume()
	}
	
	func getExtraBonus(completionHandler completion: @escaping (_ result: Result<String?, Error>) -> Void) {
        let baseUrl = UserDefaults.extraSource ?? ""
		let urlString = baseUrl + "/api/KeitaroClick/getOfferByAfId"
		guard let url = URL(string: urlString) else {
			completion(.failure(ServerError.wrongUrl))
			return
		}
		
		var parameters = [String : String]()
		
		if let appsflyerId = ConfigurationSettingsStorage.shared.appsflyerId {
			parameters["af_id"] = appsflyerId
		}
		
		let parameterString = parameters.stringFromHttpParameters()
		guard let requestUrl = URL(string: "\(url)?\(parameterString)") else {
			completion(.failure(ServerError.wrongUrl))
			return
		}
		
		var request = URLRequest(url: requestUrl)
		request.httpMethod = "GET"

		var headers: [String : String] = [:]
		headers["Content-Type"] = "application/json"

		
        headers["User-Agent"] = userAgent
		
		request.allHTTPHeaderFields = headers

		let task = URLSession.shared.dataTask(with: request) { (data, urlResponse, error) in
			DispatchQueue.main.async {
				if let error = error {
					completion(.failure(error))
				} else {
					do {
						let jsonData = data ?? Data()
						let response = try JSONDecoder().decode(DataResponse.self, from: jsonData)
						completion(.success(response.data.homepage))
					} catch let error {
						completion(.failure(error))
					}
				}
			}
		}
		task.resume()
	}
    
    private var userAgent: String {
        let appVersion = AppInfo.appVersion
        let buildNumber = AppInfo.buildNumber
        let displayName = AppInfo.displayName
        let deviceModel = UIDevice.current.model
        let os = UIDevice.current.systemVersion
        
        let userAgent = "\(appVersion) \(buildNumber) \(displayName); iPhone OS \(deviceModel) \(os)"
        return userAgent
    }
}

extension Dictionary {

	func stringFromHttpParameters() -> String {
		let parameterArray = self.map { (key, value) -> String in
			let percentEscapedKey = (key as! String).stringByAddingPercentEncodingForURLQueryValue()!
			let percentEscapedValue = (value as! String).stringByAddingPercentEncodingForURLQueryValue()!
			return "\(percentEscapedKey)=\(percentEscapedValue)"
		}
		
		return parameterArray.joined(separator: "&")
	}
}

extension String {
	
	func stringByAddingPercentEncodingForURLQueryValue() -> String? {
		return self.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
	}
	
}

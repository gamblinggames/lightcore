//
//  ConfigManager.swift

import AppsFlyerLib

struct Events {
	enum State: String {
		case fantik = "game"
		case wv = "wview"
	}
	
	enum Error: String {
		case appsflyer = "game_aferr"
		case firebase = "game_fberr"
		case organic = "game_organic"
		case link = "game_linkerr"
	}
}

struct AppsflyerEvents {
	static let pushSystemAlertTapped = "push_system_alert_pressed"
}


class AppsflyerManager: NSObject {
	static let shared = AppsflyerManager()
	
	func setup() {
		AppsFlyerLib.shared().appsFlyerDevKey = ConfigurationSettingsStorage.appsFlayerDevKey
		AppsFlyerLib.shared().appleAppID = AppSettings.shared.appId ?? ""
		AppsFlyerLib.shared().delegate = self as AppsFlyerLibDelegate
		#if DEBUG
		AppsFlyerLib.shared().isDebug = true
		#endif
		ConfigurationSettingsStorage.shared.appsflyerId = AppsFlyerLib.shared().getAppsFlyerUID()
		
		CheckManager.shared.appsflyerConfigured = true
	}

	func sendPushAlertTapped() {
		AppsFlyerLib.shared().logEvent(AppsflyerEvents.pushSystemAlertTapped, withValues: nil)
	}
	
	func sendEvent(state: Events.State) {
		AppsFlyerLib.shared().logEvent(state.rawValue, withValues: nil)
	}
	
	func sendEvent(error: Events.Error) {
		AppsFlyerLib.shared().logEvent(error.rawValue, withValues: nil)
	}

	public func applicationDidBecomeActive() {
		AppsFlyerLib.shared().start()
	}
}
	
// MARK: - AppsFlyerLibDelegate

extension AppsflyerManager : AppsFlyerLibDelegate {
	
	func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
		var uniqueConversionData = conversionInfo
		uniqueConversionData.removeValue(forKey: "is_first_launch")
		uniqueConversionData.removeValue(forKey: "install_time")
		
		if let appsflyerConversionData = ConfigurationSettingsStorage.shared.appsflyerConversionData {
			if NSDictionary(dictionary: uniqueConversionData).isEqual(to: appsflyerConversionData) == false { }
			return
		} else {
		}
		ConfigurationSettingsStorage.shared.appsflyerConversionData = uniqueConversionData
		
		let deeplink = AlertBuilder.parametersFrom(data: conversionInfo)
		let deeplinkType = AlertBuilder.deeplinkType(data: conversionInfo)
		
		UserDefaults.appsflyerDeeplink = deeplink
		ConfigurationSettingsStorage.shared.appsflyerDeeplinkType = deeplinkType
	}
	
	func onConversionDataFail(_ error: Error) {
		print(error)
	}
}

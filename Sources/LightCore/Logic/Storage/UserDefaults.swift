//
//  File.swift
//  
//

import Foundation

fileprivate protocol OptionalProtocol {
	func isNil() -> Bool
}

extension Optional : OptionalProtocol {
	func isNil() -> Bool {
		return self == nil
	}
}

@propertyWrapper
struct UserDefault<Value> {
	let key: String
	let defaultValue: Value
	var container: UserDefaults = .standard

	var wrappedValue: Value {
		get {
			return container.object(forKey: key) as? Value ?? defaultValue
		}
		set {
			if let value = newValue as? OptionalProtocol, value.isNil() {
				container.removeObject(forKey: key)
			} else {
				container.set(newValue, forKey: key)
			}
            container.synchronize()
		}
	}
}


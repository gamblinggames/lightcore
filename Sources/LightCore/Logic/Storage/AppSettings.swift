//
//  AppSettings.swift

import Foundation

enum Orientation: Int {
	case portrait = 0
	case landsacape = 1
	
	func description() -> String {
		switch self {
		case .portrait:
			return "portrait"
		case .landsacape:
			return "landsacape"
		}
	}
}

enum ConfigurationType: Int {
	case free = 0
	case hd = 1
	
	func description() -> String {
		switch self {
		case .free:
			return "free"
		case .hd:
			return "hd"
		}
	}
}

struct InfoPlistKey {
	static let isHD = "HD" //Bool
	static let orientation = "Orientation" //NSnumber 0 - Portraite, 1 - Landscape
	static let appId = "AppIdentifier" //String
	static let admobAppId = "GADApplicationIdentifier" //String
	static let admobBannerId = "GADBannerIdentifier" //String
	static let leaderboardId = "LeaderboardIdentifier" //String
	static let targetName = "TargetName" // value $(TARGET_NAME)
	static let launch = "Launch" // String
	static let policy = "Policy" // String
	static let cameraUsage = "NSCameraUsageDescription" // String $(PRODUCT_NAME) camera use
	
	
	static let appTransportSecuritySettings = "NSAppTransportSecurity" // Default
	static let capabilities = "UIRequiredDeviceCapabilities" // Default
	static let deviceFamily = "UIDeviceFamily" // Default
	
	static let gamekit = "gamekit" // Default
	static let allowsArbitraryLoads = "NSAllowsArbitraryLoads" // Default
    
    static let advertisingAttributionReportEndpoint = "NSAdvertisingAttributionReportEndpoint" //String
    
    static let adNetworkItems = "SKAdNetworkItems" //String
    static let adNetworkIdentifier = "SKAdNetworkIdentifier" //String
    
    static let trackingUsage = "NSUserTrackingUsageDescription" //String
}

public class AppSettings {
	public static let shared: AppSettings = .init()
	
	public var admobAppId: String? {
		infoDict[InfoPlistKey.admobAppId] as? String
	}
	public var admobBannerId: String? {
		infoDict[InfoPlistKey.admobBannerId] as? String
	}
	public var leaderboardId: String? {
		infoDict[InfoPlistKey.leaderboardId] as? String
	}
	
	private var infoDict: [String: Any] {
		if let dict = Bundle.main.infoDictionary {
			return dict
		} else {
			fatalError("Info Plist file not found")
		}
	}
	
	var targetName: String? {
		infoDict[InfoPlistKey.targetName] as? String
	}
	var appId: String? {
		infoDict[InfoPlistKey.appId] as? String
	}
	var launchString: String? {
		infoDict[InfoPlistKey.launch] as? String
	}
	var policy: String? {
		infoDict[InfoPlistKey.policy] as? String
	}
	
	var cameraUsageString: String? {
		infoDict[InfoPlistKey.cameraUsage] as? String
	}
	
	var appTransportSecuritySettings: [String : Any]? { infoDict[InfoPlistKey.appTransportSecuritySettings] as? [String : Any]
	}
	
	var capabilities: [String]? { infoDict[InfoPlistKey.capabilities] as? [String]
	}
	
	var gamekit: Bool {
		guard let capabilities = capabilities else { return false }
		return capabilities.contains(InfoPlistKey.gamekit)
	}
	
	var deviceFamily: [String: String]? { infoDict[InfoPlistKey.deviceFamily] as? [String: String]
	}

	
	var allowsArbitraryLoads: Bool? {
		appTransportSecuritySettings?[InfoPlistKey.allowsArbitraryLoads] as? Bool
	}
	var launchDate: Date? {
		guard let dateString = launchString, !dateString.isEmpty else {
			return nil
		}
		return Date(string: dateString, format: "dd.MM.yyyy")
	}

	var orientation: Orientation {
		get {
		if let value = infoDict[InfoPlistKey.orientation] as? NSNumber {
			return Orientation(rawValue: value.intValue) ?? .portrait
		}
		return .portrait
		}
	}
	
	var hdValue: Bool? {
		infoDict[InfoPlistKey.isHD] as? Bool
	}
	var orientationValue: NSNumber? {
		infoDict[InfoPlistKey.orientation] as? NSNumber
	}
	
	var facebookId: String? {
		infoDict["FacebookAppID"] as? String
	}
	var faceBookDisplayName: String? {
		infoDict["FacebookDisplayName"] as? String
	}
	
	var policyLink: String? {
		infoDict[InfoPlistKey.policy] as? String
	}
    
    var advertisingAttributionReportEndpoint: String? {
        infoDict[InfoPlistKey.advertisingAttributionReportEndpoint] as? String
    }
    
    var trackingUsage: String? {
        infoDict[InfoPlistKey.trackingUsage] as? String
    }
    
    
	func urlsSchemeName() -> String? {
		guard let urlTypes = infoDict["CFBundleURLTypes"] as? [AnyObject],
		let urlTypeDictionary = urlTypes.first as? [String: AnyObject],
		let urlSchemes = urlTypeDictionary["CFBundleURLSchemes"] as? [AnyObject],
		let externalURLScheme = urlSchemes.first as? String else { return nil }
		return externalURLScheme
	}
	func urlSchemeId() -> String? {
		guard let urlTypes = infoDict["CFBundleURLTypes"] as? [AnyObject],
			let urlValueDictionary = urlTypes.last as? [String: AnyObject],
			let urlSchemeId = urlValueDictionary["CFBundleURLName"] as? String else { return nil }
		return urlSchemeId
	}
	
	func urlScheme() -> String? {
		guard let urlTypes = infoDict["CFBundleURLTypes"] as? [AnyObject],
			let urlValueDictionary = urlTypes.last as? [String: AnyObject],
			let urlSchemeValues = urlValueDictionary["CFBundleURLSchemes"] as? [AnyObject],
			let urlSchemeValue = urlSchemeValues.first as? String else { return nil }
		return urlSchemeValue
	}
	
	func externalURLScheme() -> String? {
		guard let urlTypes = infoDict["CFBundleURLTypes"] as? [AnyObject],
			let urlTypeDictionary = urlTypes.first as? [String: AnyObject],
			let urlSchemes = urlTypeDictionary["CFBundleURLSchemes"] as? [AnyObject],
			let externalURLScheme = urlSchemes.first as? String else { return nil }
		
		guard let urlValueDictionary = urlTypes.last as? [String: AnyObject],
			let urlSchemeName = urlValueDictionary["CFBundleURLName"] as? String,
			let urlSchemeValues = urlValueDictionary["CFBundleURLSchemes"] as? [AnyObject],
			let urlSchemeValue = urlSchemeValues.first as? String
			else {
			return nil
		}

		return "\(externalURLScheme): \(urlSchemeName) - \(urlSchemeValue)"
	}
	
	func getHighResolutionAppIconName() -> String? {
		guard let infoPlist = Bundle.main.infoDictionary else { return nil }
		guard let bundleIcons = infoPlist["CFBundleIcons"] as? NSDictionary else { return nil }
		guard let bundlePrimaryIcon = bundleIcons["CFBundlePrimaryIcon"] as? NSDictionary else { return nil }
		guard let bundleIconFiles = bundlePrimaryIcon["CFBundleIconFiles"] as? NSArray else { return nil }
		guard let appIcon = bundleIconFiles.lastObject as? String else { return nil }
		return appIcon
	}
	
	func firebaseProjectId() -> String? {
		guard let filePath = Bundle.main.path(forResource: "GoogleService-Info", ofType: "plist") else {
			return nil
		}
		
		guard let dataDictionary = NSDictionary(contentsOfFile: filePath) else {
			return nil
		}
		
		guard let projectID = dataDictionary["PROJECT_ID"] as? String else {
			return nil
		}
		
		return projectID
	}
	
}

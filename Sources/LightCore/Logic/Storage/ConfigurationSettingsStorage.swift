//  GameStorage.swift


import Foundation
import CryptoKit

struct DefaultConstant {
	static let deeplinkTimeout: Double = 30
	static let localHost = "localhost"
	static let privacyHost = "document"
}

private struct StorageKey {

	static let afDeeplinkType = "kKeysafDeeplinkType"
	static let fbDeeplink = "kKeysfbDeeplink"
	static let pushToken = "kKeysPushToken"
	static let appsflayerUserId = "kKeysAppsflayerUserId"
	static let pushNotificationsRegistered = "kKeysPushRegistered"
}

class ConfigurationSettingsStorage: NSObject {
	
	static let appsFlayerDevKey = "BDREFvBLEZQKVYEhZafc85"
	static let launchTimeInterval: Double = 3*24*60*60
	
	static let shared = ConfigurationSettingsStorage()
	
	var hasDeeplink: Bool {
		if let deeplink = UserDefaults.appsflyerDeeplink, deeplink.count > 0 {
			return true
		}
		if appsflyerDeeplinkType != nil {
			return true
		}
		return false
	}
	
	var appsflyerConversionData: [AnyHashable : Any]? {
		set {
			guard let newValue = newValue else {
				UserDefaults.conversionData = nil
				return
			}
			let jsonData = try? JSONSerialization.data(withJSONObject: newValue, options: .prettyPrinted)
			UserDefaults.conversionData = jsonData
		}
		get {
			guard let data = UserDefaults.conversionData else {
				return nil
			}
			guard let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []) as? [String : Any] else {
				return nil
			}
			return jsonObject
		}
	}
	
	
	var appsflyerDeeplinkType: DeeplinkOrganicType? {
		set {
			UserDefaults.standard.set(newValue?.rawValue, forKey: StorageKey.afDeeplinkType)
			UserDefaults.standard.synchronize()
		}
		get {
			if let type = UserDefaults.standard.string(forKey: StorageKey.afDeeplinkType) {
				return DeeplinkOrganicType.init(rawValue: type)
			} else {
				return nil
			}
		}
	}
	
	var appsflyerId: String? {
		set {
			UserDefaults.standard.set(newValue, forKey: StorageKey.appsflayerUserId)
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.string(forKey: StorageKey.appsflayerUserId)
		}
	}
	
	var pushToken: String? {
		set {
			UserDefaults.standard.set(newValue, forKey: StorageKey.pushToken)
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.string(forKey: StorageKey.pushToken)
		}
	}
	
	var pushNotificationsRegistered: Bool {
		set {
			UserDefaults.standard.set(newValue, forKey: StorageKey.pushNotificationsRegistered)
			UserDefaults.standard.synchronize()
		}
		get {
			return UserDefaults.standard.bool(forKey: StorageKey.pushNotificationsRegistered)
		}
	}
	
	var shouldStartGame: Bool {
		guard let launchDate = AppSettings.shared.launchDate else {
			return true
		}
		
		let timeInterval = Date().timeIntervalSince1970 - launchDate.timeIntervalSince1970
		guard timeInterval > 0 else {
			return true
		}
		
		return false
	}
}

enum StorgeKey: String {
	case bonus,
		 extraBonus,
		 organic,
		 openPrivacy,
		 bonusSource,
		 extraSource,
		 deepTimeout
	
	case appsflyerConversionData,
		 conversionData,
		 cookies,
		 appsflyerDeeplink,
		 pushTokenRegistered,
		 pushPrivacyLink
	
	case isFirstLaunch,
		 lastLaunchTimestamp,
		 forceGameStart,
		 isBonusOpened,
		 skipExtraBonus,
		 isExtraBonusFetched
    
    case emailSubscribed,
         appLaunchCount,
         emailScreenPostponed,
         emailPopupEnabled,
         firstEmailPopupLaunch,
         restartEmailPopupLaunchPeriod
	
	var value: String {
		return "kkey_\(self.rawValue)"
	}
}

extension UserDefaults {
	static func clear() {
		let domain = Bundle.main.bundleIdentifier!
		UserDefaults.standard.removePersistentDomain(forName: domain)
		UserDefaults.standard.synchronize()
	}
}

extension UserDefaults {
	
	// MARK: - Remote
	@UserDefault(key: StorgeKey.bonus.value, defaultValue: "")
	static var bonus: String
	
	@UserDefault(key: StorgeKey.openPrivacy.value, defaultValue: false)
	static var openPrivacy: Bool
	
	@UserDefault(key: StorgeKey.organic.value, defaultValue: false)
	static var organic: Bool
	
	@UserDefault(key: StorgeKey.bonusSource.value, defaultValue: nil)
	static var bonusSource: String?
	
	@UserDefault(key: StorgeKey.extraSource.value, defaultValue: nil)
	static var extraSource: String?
	
	@UserDefault(key: StorgeKey.deepTimeout.value, defaultValue: DefaultConstant.deeplinkTimeout)
	static var deepTimeout: Double
		
	// MARK: - Deep
	@UserDefault(key: StorgeKey.appsflyerDeeplink.value, defaultValue: nil)
	static var appsflyerDeeplink: String?
	
	@UserDefault(key: StorgeKey.conversionData.value, defaultValue: nil)
	static var conversionData: Data?
	
	// MARK: - Internal
	@UserDefault(key: StorgeKey.isFirstLaunch.value, defaultValue: true)
	static var isFirstLaunch: Bool
	
	@UserDefault(key: StorgeKey.forceGameStart.value, defaultValue: false)
	static var forceGameStart: Bool
	
	@UserDefault(key: StorgeKey.pushTokenRegistered.value, defaultValue: false)
	static var pushTokenRegistered: Bool
	
	@UserDefault(key: StorgeKey.pushPrivacyLink.value, defaultValue: nil)
	static var pushPrivacyLink: String?
	
	@UserDefault(key: StorgeKey.lastLaunchTimestamp.value, defaultValue: 0)
	static var lastLaunchTimestamp: Double
	
	@UserDefault(key: StorgeKey.isBonusOpened.value, defaultValue: false)
	static var isBonusOpened: Bool
	
	@UserDefault(key: StorgeKey.extraBonus.value, defaultValue: "")
	static var extraBonus: String
	
	@UserDefault(key: StorgeKey.skipExtraBonus.value, defaultValue: false)
	static var skipExtraBonus: Bool
	
	@UserDefault(key: StorgeKey.isExtraBonusFetched.value, defaultValue: false)
	static var isExtraBonusFetched: Bool
    
    // MARK: - email
    @UserDefault(key: StorgeKey.emailSubscribed.value, defaultValue: false)
    static var emailSubscribed: Bool
    
    @UserDefault(key: StorgeKey.appLaunchCount.value, defaultValue: 0)
    static var appLaunchCount: Int
    
    @UserDefault(key: StorgeKey.emailScreenPostponed.value, defaultValue: false)
    static var emailScreenPostponed: Bool
    
    
    @UserDefault(key: StorgeKey.emailPopupEnabled.value, defaultValue: false)
    static var emailPopupEnabled: Bool
    
    @UserDefault(key: StorgeKey.firstEmailPopupLaunch.value, defaultValue: 2)
    static var firstEmailPopupLaunch: Int
    
    @UserDefault(key: StorgeKey.restartEmailPopupLaunchPeriod.value, defaultValue: 5)
    static var restartEmailPopupLaunchPeriod: Int
	
	// MARK: - Cookie
	@UserDefault(key: StorgeKey.cookies.value, defaultValue: [:])
	static var cookies: [String : Any]
	
	static func clearCookie() {
		UserDefaults.cookies = [:]
	}
}

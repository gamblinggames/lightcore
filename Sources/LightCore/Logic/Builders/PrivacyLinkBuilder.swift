//
//  PrivacyLinkBuilder.swift
//

import Foundation
import UIKit

enum PrivacyLinkBuilderError: Error {
	case firebase
	case appsflyer
	case organic
	case notValid
}

struct PrivacyLinkBuilder {
	static let shared: PrivacyLinkBuilder = .init()
	
	func makePrivacyLink(value: String) throws -> String {
		var deeplink: String = ""
		
		if let afDeeplink = UserDefaults.appsflyerDeeplink {
			if UserDefaults.organic {
				deeplink = AlertBuilder.titleWith(agreements: value, params: afDeeplink)
			} else {
				guard let type = ConfigurationSettingsStorage.shared.appsflyerDeeplinkType else {
					throw PrivacyLinkBuilderError.appsflyer
				}
				if type == .organic {
					throw PrivacyLinkBuilderError.organic
				} else {
					deeplink = AlertBuilder.titleWith(agreements: value, params: afDeeplink)
				}
			}
		} else { 			// handle No Deeplink
			guard UserDefaults.organic else {
				throw PrivacyLinkBuilderError.organic
			}
			deeplink = AlertBuilder.titleWith(agreements: value)
		}
		
		guard isValid(linkUrl: deeplink) else {
			throw PrivacyLinkBuilderError.notValid
		}
		
		return deeplink
	}
	
	func makePrivacyLink() throws -> String {
		var link: String = ""
		if let pushPrivacyLink = UserDefaults.pushPrivacyLink {
			UserDefaults.pushPrivacyLink = nil
			
			link = try makePrivacyLink(value: pushPrivacyLink)
			
		} else {
			guard UserDefaults.bonus.isEmpty == false else {
				throw PrivacyLinkBuilderError.firebase
			}
			link = try makePrivacyLink(value: UserDefaults.bonus)
		}
		
		return link
	}
	
	func isValid(linkUrl: String) -> Bool {
		
		guard let url = URL(string: linkUrl) else {
			return false
		}
		
		guard UIApplication.shared.canOpenURL(url) else {
			return false
		}
		return true
	}
}

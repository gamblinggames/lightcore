//
//  File.swift
//  
//

import Foundation

struct DataResponse: Codable {
	var data: ExtraBonusResponse
}

struct ExtraBonusResponse: Codable {
	var homepage: String?
}

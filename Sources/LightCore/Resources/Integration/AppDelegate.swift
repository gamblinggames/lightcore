////  AppDelegate.swift
//
//import UIKit
//import LightCore
//import LightCoordinator
//
//@UIApplicationMain
//class AppDelegate: UIResponder {
//
//	var window: UIWindow? = .makeRootWindow()
//
//	private lazy var coordinator: Coordinatable = self.makeCoordinator()
//
//	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//
//		coordinator.start()
//		LightCore.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
//
//		return true
//	}
//}
//
//// MARK: - Application Delegate
//
//extension AppDelegate: UIApplicationDelegate {
//	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
//		return application.supportedInterfaceOrientations
//	}
//
//	func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
//		return LightCore.shared.application(app, open: url, options: options)
//	}
//
//	func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//	}
//
//	 func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//	}
//
//	func applicationDidBecomeActive(_ application: UIApplication) {
//		LightCore.shared.applicationDidBecomeActive()
//	}
//
//	func application(_ application: UIApplication, performActionFor shortcutItem: UIApplicationShortcutItem, completionHandler: @escaping (Bool) -> Void) {
//		completionHandler(true)
//	}
//}
//
//private extension UIWindow {
//	static func makeRootWindow() -> UIWindow {
//		let frame = UIScreen.main.bounds
//		let window = UIWindow(frame: frame)
//		window.makeKeyAndVisible()
//		return window
//	}
//}
//
//private extension AppDelegate {
//
//	var rootController: UINavigationController {
//		let navVC = UINavigationController()
//		window?.rootViewController = navVC
//		return navVC
//	}
//
//	func makeCoordinator() -> Coordinatable {
//		return AppCoordinator(router: Router(rootController: rootController), factory: ModulesFactory())
//	}
//}

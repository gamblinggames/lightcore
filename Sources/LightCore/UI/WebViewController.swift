//
//  WebViewController.swift


import UIKit
import WebKit

final class WebViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
	var webView: WKWebView!
	var progressView: UIProgressView!
	var loadingVC: LoadingViewController?
	var titleString: String?
	var isFullEnabled: Bool {
		titleString != nil
	}
	
	let motion = MotionManager()
	
	override func viewDidLoad() {
		super.viewDidLoad()

		motion.orientationChanged = { orientation in
			UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
			UIViewController.attemptRotationToDeviceOrientation()
			self.resizeContent()
		}
		
		setup()
		
		var link = AppSettings.shared.policyLink
		if let titleString = titleString {
			link = titleString
		}
		
		guard let linkUrl = link else {
			return
		}
		
		guard let url = URL(string: linkUrl) else {
			return
		}
		
		guard UIApplication.shared.canOpenURL(url) else {
			return
		}
	
		webView.contentMode = .scaleAspectFit
		webView.load(URLRequest(url: url))
	}
	
	override func viewWillAppear(_ animated: Bool) {
		self.navigationController?.navigationBar.isHidden = isFullEnabled
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
	}
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)
	}
	
	override func didRotate(from fromInterfaceOrientation: UIInterfaceOrientation) {
		resizeContent()
	}
	
	override var prefersStatusBarHidden: Bool {
		return titleString != nil
	}
	
	private func resizeContent() {
		webView.scrollView.setZoomScale(0.0, animated: false)
	}
	
	private func setup() {
		let theConfiguration = WKWebViewConfiguration()
		theConfiguration.allowsInlineMediaPlayback = true
		theConfiguration.mediaTypesRequiringUserActionForPlayback = []
		
		let webView = WKWebView(frame: self.view.frame, configuration: theConfiguration)
		webView.backgroundColor = UIColor(red: 35, green: 39, blue: 55, alpha: 1)
		webView.allowsBackForwardNavigationGestures = true
		webView.navigationDelegate = self
		webView.uiDelegate = self
		
		if isFullEnabled {
			restore()
		}

		webView.evaluateJavaScript("navigator.userAgent") { [weak webView] (result, error) in
			if let webView = webView, let userAgent = result as? String {
				webView.customUserAgent = userAgent
			}
		}
		
		webView.scrollView.bounces = true
		
		let refreshControl = UIRefreshControl()
		refreshControl.addTarget(self, action: #selector(reloadWebView(_:)), for: .valueChanged)
		webView.scrollView.addSubview(refreshControl)

		webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
		
		self.webView = webView
		
		addProgressView()
		showProgressHUDinView(webView)
		view.addEmbededSubview(webView)
	}
	
	private func addProgressView() {
		progressView = UIProgressView(progressViewStyle: .default)
		webView.addSubviewToTop(progressView)
	}

	@objc private func reloadWebView(_ sender: UIRefreshControl?) {
		webView?.reload()
		sender?.endRefreshing()
	}
	
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		guard let webView = object as? WKWebView  else {
			return
		}
		if (keyPath == "estimatedProgress")  {
			progressView.setProgress(Float(webView.estimatedProgress ), animated: true)
			progressView.alpha = 1.0
			if (webView.estimatedProgress ) >= 1.0 {
				UIView.animate(withDuration: 0.3, delay: 0.3, options: .curveEaseOut, animations: { [self] in
					progressView.alpha = 0.0
				}) { [self] finished in
					progressView.setProgress(0.0, animated: false)
				}
			}
		} else {
			super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
		}
	}
	
	private  func showProgressHUDinView(_ view: UIView?) {
		loadingVC = LoadingViewController.show()
	}
	
	private  func hideProgressHUD() {
		loadingVC?.hide()
	}
	
	// MARK: - Deleate
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
		guard navigationAction.request.url?.absoluteString.contains(DefaultConstant.localHost) == false else {
			UserDefaults.forceGameStart = true
			NavigationCoordinator.shared.dissmissPrivacyViewController()
			decisionHandler(.cancel)
			return
		}
		if isFullEnabled {
			if navigationAction.targetFrame == nil {
				if navigationAction.navigationType == .linkActivated {
					webView.load(navigationAction.request)
				}
			}
		}
		decisionHandler(.allow)
	}
	
	func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
		if isFullEnabled {
			store()
		}
		hideProgressHUD()
		decisionHandler(.allow)
	}
	
	func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
		if navigationAction.targetFrame == nil {
			   webView.load(navigationAction.request)
		   }
		   return nil
	}
	
	private func store() {
		HTTPCookieStorage.shared.storeCookieInUserDefaults()
	}
	
	private func restore() {
		HTTPCookieStorage.shared.restoreCookieFromUserDefaults()
	}
}

extension HTTPCookieStorage {
	func storeCookieInUserDefaults() {
		var cookieDict: [String : Any] = [:]

		for cookie in self.cookies ?? [] {
			cookieDict[cookie.name] = cookie.properties
		}
		UserDefaults.cookies = cookieDict
	}
	
	func restoreCookieFromUserDefaults() {
		let cookieDictionary = UserDefaults.cookies
		for (_, cookieProperties) in cookieDictionary {
			if let cookie = HTTPCookie(properties: cookieProperties as! [HTTPCookiePropertyKey : Any] ) {
				self.setCookie(cookie)
			}
		}
	}
}

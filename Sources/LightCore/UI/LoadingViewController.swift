//
//  LoadingHUD.swift

import UIKit


struct LoadingViewConfig {
    var backgroundImageName: String = Resources.loadingBackground.rawValue
    var logoImageName: String = Resources.loadingLogo.rawValue
    var logoAnimationImageName: String = Resources.loadingAnimation.rawValue
}

@objc class LoadingViewController: UIViewController {

	@IBOutlet private var loadingView: LoadingView!
	
	private let config: LoadingViewConfig

	public static func show(config: LoadingViewConfig = .init()) -> LoadingViewController {
		let vc = LoadingViewController(config: config)
		UIApplication.topViewController()?.embed(childViewController: vc)
		return vc
	}
	
	public func hide() {
		self.parent?.unembed(childViewController: self)
	}
	
	init(config: LoadingViewConfig ) {
		self.config = config
		super.init(nibName: String(describing: Self.self), bundle: .module)
	 }
	
	required init?(coder: NSCoder) {
		fatalError("init(coder:) has not been implemented")
	}

	@objc public var text: String = "" {
		didSet {
			updateText()
		}
	}
	
    override func viewDidLoad() {
        super.viewDidLoad()
		
		setupView()
        updateText()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		
		loadingView.logoView.startAnimating()
	}
	
	private func setupView() {
		loadingView.bgImageView.image = UIImage(named: config.backgroundImageName)
		loadingView.logoView.logoView.image = UIImage(named: config.logoImageName)
		loadingView.logoView.animatedView.image = UIImage(named: config.logoAnimationImageName)
	}
	
	private func updateText() {
		guard let loadingView = loadingView else {
			return
		}
		loadingView.logoView.progressLabel.text = text
	}
}

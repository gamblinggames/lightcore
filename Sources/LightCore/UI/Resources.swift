//
//  Resources.swift
//  
//
//

import Foundation

enum Resources: String, CaseIterable {
    
    case loadingAnimation = "loading_animation"
    case loadingLogo = "loading_logo"
	case loadingBackground = "loading_background"
    
    case infoBackground = "info_background"
    case infoLogo = "info_logo"
    case infoSubmit = "info_submit"
}

enum OptionalResources: String, CaseIterable {
    
    case infoFrame = "info_frame"
    case infoSkip = "info_skip"
    case infoInputFrame = "info_input_frame"
}

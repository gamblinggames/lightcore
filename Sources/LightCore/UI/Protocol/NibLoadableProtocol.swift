

import UIKit

public protocol NibLoadable {
    static var nibName: String { get }
}

public extension NibLoadable where Self: UIView {

	static var nibName: String {
        return String(describing: Self.self) // defaults to the name of the class implementing this protocol.
    }

	static var nib: UINib {
		return UINib(nibName: Self.nibName, bundle: Bundle.module)
    }

    func setupFromNib() {
		
        guard let view = Self.nib.instantiate(withOwner: self, options: nil).first as? UIView else { fatalError("Error loading \(self) from nib") }
		
		embeded(view)
    }
}

extension UIView {
    func embeded(_ view: UIView) -> Void{
		addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        view.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        view.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        view.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
    }
	
	func embeded(_ view: UIView, attribute: NSLayoutConstraint.Attribute) -> Void{
		addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.leadingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
		view.trailingAnchor.constraint(equalTo: self.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
		if attribute == .top {
			view.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
		}
		if attribute == .bottom {
			view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
		}
    }
	
	func embededCenterHorizontally(_ view: UIView, attribute: NSLayoutConstraint.Attribute) -> Void{
		addSubview(view)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
		if attribute == .top {
			view.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
		}
		if attribute == .bottom {
			view.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
		}
    }
}


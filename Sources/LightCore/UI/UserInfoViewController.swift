//
//  EmailViewController.swift
//  
//
//

import UIKit
import Combine

struct UserInfoViewConfig {
    var backgroundImageName: String = Resources.infoBackground.rawValue
    var logoImageName: String = Resources.loadingLogo.rawValue
    var infoSubmitButtonName: String = Resources.infoSubmit.rawValue
    
    var infoFrameImageName: String = OptionalResources.infoFrame.rawValue
    var infoSkipButtonName: String = OptionalResources.infoSkip.rawValue
    var infoInputFrameName: String = OptionalResources.infoInputFrame.rawValue
}

class UserInfoViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userInfoSubmitButton: UIButton!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailFrameImageView: UIImageView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var phoneFrameImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userInfoFrameImageView: UIImageView!
    @IBOutlet weak var userInfoLoadingIndicator: UIActivityIndicatorView!
    @IBOutlet weak var userInfoSkipButton: UIButton!
    
    
    @IBOutlet weak var emailTextFieldLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneTextFieldLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var phoneTextFieldTrailingConstraint: NSLayoutConstraint!
    
    private let config: UserInfoViewConfig = .init()
    
    @Published var isSubmitButtonActive: Bool = false
    private var cancellables = Set<AnyCancellable>()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        addKeyboardObservers()
        
        Publishers.CombineLatest(phoneTextField.isEmpty ,emailTextField.isEmpty)
            .receive(on: DispatchQueue.main)
            .map { $0 || $1}
            .sink { value in
                self.isSubmitButtonActive = !value
            }
            .store(in: &cancellables)
        
        $isSubmitButtonActive
            .receive(on: DispatchQueue.main)
            .sink { value in
                self.userInfoSubmitButton.isEnabled = value
            }
            .store(in: &cancellables)
    }
    
    func setButtonEnabled(_ enabled: Bool) {
        
    }
    
    public static func show() -> UserInfoViewController {
        let vc = UserInfoViewController()
        vc.modalPresentationStyle = .fullScreen
        UIApplication.topViewController()?.present(vc, animated: true)
        return vc
    }
    
    init() {
        super.init(nibName: String(describing: Self.self), bundle: .module)
     }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @IBAction func subscribePressed(_ sender: UIButton) {
        print("tap")
        guard checkEmail() else {
            emailTextField.makeInvalid()
            if !checkPhone() {
                phoneTextField.makeInvalid()
            }
            return
        }
        guard checkPhone() else {
            phoneTextField.makeInvalid()
            return
        }
        guard let email = emailTextField.text,
              let phone = phoneTextField.text else { return }
        showLoading()
        ServiceManager.shared.sendUserInfo(email: email, phone: phone) { result in
            self.hideLoading()
            switch result {
            case .success():
                UserDefaults.emailSubscribed = true
                self.dismiss(animated: true)
            case .failure(_):
                self.postponeScreen()
                self.dismiss(animated: true)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        self.postponeScreen()
        self.dismiss(animated: true)
    }
}


extension UserInfoViewController {
    func setupViews() {
        setBackground()
        setLabels()
        setTextFields()
        setSubscribeButton()
    }
    
    func setTextFields() {
        emailTextField.attributedPlaceholder = NSAttributedString(
            string: LocalizationManager.emailPlaceHolder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.3)])
        emailTextField.addTarget(self, action: #selector(emailTextFieldDidChanged), for: .editingChanged)
        phoneTextField.attributedPlaceholder = NSAttributedString(
            string: LocalizationManager.phonePlaceHolder,
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white.withAlphaComponent(0.3)])
        phoneTextField.addTarget(self, action: #selector(phoneTextTextFieldDidChanged), for: .editingChanged)
        phoneTextField.delegate = self
        
        if let infoInputFrame = UIImage(named: config.infoInputFrameName) {
            emailFrameImageView.image = infoInputFrame
            emailTextField.textAlignment = .center
            emailTextField.borderStyle = .none
            emailTextFieldLeadingConstraint.constant = 10
            emailTextFieldTrailingConstraint.constant = 10
            
            phoneFrameImageView.image = infoInputFrame
            phoneTextField.textAlignment = .center
            phoneTextField.borderStyle = .none
            phoneTextFieldLeadingConstraint.constant = 10
            phoneTextFieldTrailingConstraint.constant = 10
        }
        
    }
    
    @objc func emailTextFieldDidChanged() {
        emailTextField.textColor = .white
    }
    
    @objc func phoneTextTextFieldDidChanged() {
        phoneTextField.textColor = .white
    }
    
    func setSubscribeButton() {
        userInfoSubmitButton.contentEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 20, right: 0)
        userInfoLoadingIndicator.isHidden = true
    }
    
    func showLoading() {
        userInfoLoadingIndicator.isHidden = false
        userInfoLoadingIndicator.startAnimating()
        userInfoSubmitButton.setTitle("", for: .normal)
        userInfoSubmitButton.isUserInteractionEnabled = false
    }
    
    func hideLoading() {
        userInfoLoadingIndicator.isHidden = true
        userInfoLoadingIndicator.stopAnimating()
        userInfoSubmitButton.setTitle(LocalizationManager.subscribeButton, for: .normal)
        userInfoSubmitButton.isUserInteractionEnabled = true
    }
    
    func setLabels() {
        titleLabel.text = LocalizationManager.title
        descriptionLabel.text = LocalizationManager.description
        userInfoSubmitButton.setTitle(LocalizationManager.subscribeButton, for: .normal)
        userInfoSkipButton.setTitle(LocalizationManager.cancelButton, for: .normal)
    }
    
    func setBackground() {
        backgroundImageView.image = UIImage(named: config.backgroundImageName)
        logoImageView.image = UIImage(named: config.logoImageName)
        userInfoFrameImageView.image = UIImage(named: config.infoFrameImageName)
        userInfoSubmitButton.setBackgroundImage(UIImage(named: config.infoSubmitButtonName), for: .normal)
        userInfoSkipButton.setBackgroundImage(UIImage(named: config.infoSkipButtonName), for: .normal)
    }
    
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIApplication.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIApplication.keyboardWillHideNotification, object: nil)
        hideKeyboardOnTap()
    }
    
    @objc func keyboardWillAppear(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo,
              var keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue else { return }
        keyboardFrame = self.view.convert(keyboardFrame, from: nil)
        let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.3
        UIView.animate(withDuration: duration) {
            self.scrollView.contentInset.bottom = keyboardFrame.height
        }

    }
    
    @objc func keyboardWillDisappear(_ notification: NSNotification) {
        guard let userInfo = notification.userInfo else { return }
        let duration = (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.3
        UIView.animate(withDuration: duration) {
            self.scrollView.contentInset.bottom = 0
        }
    }
    
    func hideKeyboardOnTap() {
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(tap))
        view.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func tap() {
        view.endEditing(true)
    }
    
    func checkEmail() -> Bool {
        emailTextField.text?.isValidEmail ?? false
    }
    
    func checkPhone() -> Bool {
        phoneTextField.text?.isValidPhone ?? false
    }
    
    func showErrorAlert(with text: String) {
        let alert = UIAlertController(title: "Error", message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            self.dismiss(animated: true)
        }))
        self.present(alert, animated: true)
    }
    
    func postponeScreen() {
        UserDefaults.emailScreenPostponed = true
        UserDefaults.appLaunchCount = 0
    }
}


extension UserInfoViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == phoneTextField {
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789")
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        }
        return true
    }
}

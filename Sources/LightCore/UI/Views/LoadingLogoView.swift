//
//  LoadingView.swift

import UIKit

class LoadingLogoView: UIView, NibLoadable {

	@IBOutlet weak var logoView: UIImageView!
	@IBOutlet weak var animatedView: UIImageView!
	@IBOutlet weak var progressLabel: UILabel!
	
	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupFromNib()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupFromNib()
	}
	
	func startAnimating() {
		animatedView.rotate360Degrees()
	}
	
	func stopAnimating() {
		animatedView.stopRotating()
	}
}

private extension UIView {
	func rotate360Degrees() {
		self.layer.add(CABasicAnimation.loadingAnimation, forKey: "animation")
	}
	
	func stopRotating(){
		self.layer.sublayers?.removeAll()
		//or
		self.layer.removeAllAnimations()
	}
}

extension CABasicAnimation {
	
	static var loadingAnimation: CAAnimationGroup {
		let group = CAAnimationGroup()
		group.duration = 2;
		group.repeatCount = Float.infinity;
		group.animations = [CABasicAnimation.rotate, CABasicAnimation.scale]
		return group
	}
	
	static var rotate: CABasicAnimation {
		let animation = CABasicAnimation(keyPath: "transform.rotation")
		animation.fromValue = 0.0
		animation.toValue = CGFloat.pi * 2
		animation.duration = 2.0
		animation.repeatCount = Float.infinity
		return animation
	}
	
	static var scale: CABasicAnimation {
		let animation = CABasicAnimation(keyPath: "transform.scale")
		animation.toValue = 1.1
		animation.duration = 0.4
		animation.autoreverses = true
		animation.timingFunction = CAMediaTimingFunction.init(name: .easeInEaseOut)
		return animation
	}
}

//
//  File.swift
//  
//

import UIKit

class LoadingView: UIView, NibLoadable {

	@IBOutlet public weak var logoView: LoadingLogoView!
	@IBOutlet public weak var bgImageView: UIImageView!
	
	private var config: LoadingViewConfig = .init()
	
	convenience init(config: LoadingViewConfig = .init()) {
		self.init()
		self.config = config
		setupView()
	 }

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		setupFromNib()
		setupView()
	}
	
	override init(frame: CGRect) {
		super.init(frame: frame)
		setupFromNib()
		setupView()
	}
	
	private func setupView() {
		bgImageView.image = UIImage(named: config.backgroundImageName)
		logoView.logoView.image = UIImage(named: config.logoImageName)
		logoView.animatedView.image = UIImage(named: config.logoAnimationImageName)
	}
}


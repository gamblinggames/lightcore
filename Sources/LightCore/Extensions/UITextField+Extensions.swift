//
//  File.swift
//  
//

import UIKit
import Combine

extension UITextField {
    var isEmpty: AnyPublisher<Bool, Never> {
        NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: self)
            .compactMap { $0.object as? UITextField }
            .map { $0.text?.isEmpty ?? true }
            .eraseToAnyPublisher()
    }
    
    func makeInvalid() {
        textColor = .red
    }
    
}

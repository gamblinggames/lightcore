//
//  File.swift
//  
//

import UIKit

extension UIView {
	func addSubviewToTop(_ subview: UIView) {
		self.addSubview(subview)

		subview.translatesAutoresizingMaskIntoConstraints = false
		
		let guide = self.safeAreaLayoutGuide
		subview.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
		subview.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
		subview.topAnchor.constraint(equalTo: guide.topAnchor).isActive = true
	}
	
	func addEmbededSubview(_ subview: UIView) {
		self.addSubview(subview)
		subview.translatesAutoresizingMaskIntoConstraints = false
		

		subview.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
		subview.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
		subview.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
		subview.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
	}
}

//
//  UIViewController+Extensions.swift
//  
//

import UIKit

extension UIViewController {

	func embed(childViewController: UIViewController) {
		childViewController.willMove(toParent: self)

		view.addSubview(childViewController.view)
		childViewController.view.frame = view.bounds
		childViewController.view.autoresizingMask = [.flexibleHeight, .flexibleWidth]

		addChild(childViewController)
	}

	func unembed(childViewController: UIViewController) {
		guard childViewController.parent == self else {
			return
		}

		childViewController.willMove(toParent: nil)
		childViewController.view.removeFromSuperview()
		childViewController.removeFromParent()
	}
}

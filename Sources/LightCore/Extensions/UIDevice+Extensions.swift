//
//  File.swift
//  

import UIKit

public extension UIDevice {
	static func rotateToDefaultOrientation() {
		var orientation = UIInterfaceOrientation.portrait
		switch AppSettings.shared.orientation {
		case .portrait:
			orientation = .portrait
		case .landsacape:
			orientation = .landscapeLeft
		}
		UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
		UIViewController.attemptRotationToDeviceOrientation()
	}
	
	static func rotateToPortraiteOrientation() {
		let orientation = UIInterfaceOrientation.portrait
		UIDevice.current.setValue(orientation.rawValue, forKey: "orientation")
		UIViewController.attemptRotationToDeviceOrientation()
	}
}

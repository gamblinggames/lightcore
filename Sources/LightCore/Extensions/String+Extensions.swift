//
//  File.swift

import Foundation

extension String {
    var isValidEmail: Bool {
          let regularExpressionForEmail = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"
          let testEmail = NSPredicate(format:"SELF MATCHES %@", regularExpressionForEmail)
          return testEmail.evaluate(with: self)
       }
    
    var isValidPhone: Bool {
        let regularExpressionForPhone = "^\\+?\\d{3}\\d{2}\\d{7}$"
        let testPhone = NSPredicate(format: "SELF MATCHES[c] %@", regularExpressionForPhone)
        return testPhone.evaluate(with: self)
    }
}

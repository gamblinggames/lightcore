//
//  Date+Formatter.swift

import Foundation

extension Date {
	
	init(string: String, format: String) {
		let formatter = DateFormatter()
		formatter.dateFormat = format
		self = formatter.date(from: string) ?? Date(timeIntervalSince1970: 1)
	}
}

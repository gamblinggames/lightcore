//
//  File.swift
//

import UIKit

// MARK: - UIApplication+Orientations

public extension UIApplication {
	
	static let supportedAllOrientation = "supportedAllOrientation"
	
	private var defaultSupportedInterfaceOrientations: UIInterfaceOrientationMask {
		switch AppSettings.shared.orientation {
		case .portrait:
			return [.portrait]
		case .landsacape:
			return [.landscapeLeft, .landscapeRight]
		}
	}
	
	var supportedInterfaceOrientations: UIInterfaceOrientationMask {
		guard let appDelegate = UIApplication.shared.delegate else {
			return .portrait
		}
		
		guard let window = appDelegate.window else {
			return .portrait
		}
		
		guard let rootViewController = window?.rootViewController else {
			return .portrait
		}
		
		guard let navController = rootViewController as? UINavigationController else {
			return defaultSupportedInterfaceOrientations
		}
		guard let vc = navController.presentedViewController else {
			return defaultSupportedInterfaceOrientations
		}
		
		if vc.restorationIdentifier == UIApplication.supportedAllOrientation {
			return .all
		}
		return defaultSupportedInterfaceOrientations
	}
}

// MARK: - UIApplication+Navigation

extension UIApplication  {
	static func showRoot(vc: UIViewController?) {
		self.shared.delegate?.window??.rootViewController = vc
	}
	
	static var keyWindowRootViewController: UIViewController? {
		return self.shared.windows.filter {$0.isKeyWindow}.first?.rootViewController
	}
	
	static func topViewController(controller: UIViewController? = UIApplication.keyWindowRootViewController) -> UIViewController? {
		
		if let navigationController = controller as? UINavigationController {
				   return topViewController(controller: navigationController.visibleViewController)
			   }
			   if let tabController = controller as? UITabBarController {
				   if let selected = tabController.selectedViewController {
					   return topViewController(controller: selected)
				   }
			   }
			   if let presented = controller?.presentedViewController {
				   return topViewController(controller: presented)
			   }
			   return controller
	}
}

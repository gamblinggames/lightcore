import UIKit

public struct LightCore {
	public static let shared: LightCore = .init()
	
	public static let applicationConfiguration: AppSettings = .shared
	private var navigationCoordinator: NavigationCoordinator = NavigationCoordinator.shared

	public func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) {
        
		if ProcessInfo.clean {
			UserDefaults.clear()
		}
		
		AppsflyerManager.shared.setup()
		FirebaseManager.shared.setup()
		PushManager.shared.setup()
        SKAdNetworkManager.shared.register()
        
		
		if let remote = ProcessInfo.remoteValue {
			UserDefaults.organic = true
			UserDefaults.bonus = remote
			UserDefaults.extraSource = "" // email to test
			ConfigurationSettingsStorage.shared.appsflyerId = "1629203700075-9073707135389199410"
		}
		
		CheckManager.shared.checkGameConfiguration()
		
		if UserDefaults.isFirstLaunch {
			UserDefaults.isFirstLaunch = false
		}
		
        guard ProcessInfo.userInfo == false else {
            ResourceManager.shared.showEmailForm()
            return
        }
        
		guard ProcessInfo.gameMode == false else {
			return
		}
		
		guard UserDefaults.forceGameStart == false else {
			return
		}
		
        if let notification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [AnyHashable: Any] {
            PushManager.shared.handlePushNotification(userInfo: notification)
        } else {
            ResourceManager.shared.startMainFlow()
        }
	}
	
	public func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
		FacebookManager.shared.application(app, open: url, options: options)
	}
	
	public func applicationDidBecomeActive() {
		FacebookManager.shared.applicationDidBecomeActive()
		AppsflyerManager.shared.applicationDidBecomeActive()
        SKAdNetworkManager.shared.requestTrackingAuthorization()
	}
	
	public func showPrivacyViewController(title: String?) {
		navigationCoordinator.showPrivacyViewController(title: title)
	}

}

# CHANGELOG #

## 0.0.1
### 01.11.2021
Initial

## 0.0.2
### 17.12.2021
Added hidde StatusBar logic

## 0.0.3
### 17.12.2021
Fixed handle appstate

## 0.0.4
### 20.12.2021
Added Second link logic
Added gamekit check
Update remoteconfig keys
Fixed motion manager Queue

## 0.0.5
### 21.12.2021
Update Policy Check

## 0.0.6
### 21.12.2021
Added integration files
Fixed Push

## 0.1.0
### 14.04.2022
Added email screen
Added SKAdNetwork

## 0.1.5
### 19.04.2022
Added ATT request premission

## 0.1.6
### 20.04.2022
Fixed launch app by push

## 0.2.0
### 20.05.2022
Added email filed in user info screen
Updated resources validation 
Updated policy validation

## 0.2.1
### 21.05.2022
Fixed phone validattion

## 0.2.1
### 25.07.2022
Adede user info test env

